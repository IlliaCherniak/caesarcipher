﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab1
{
    class FrequencyAnalysis
    {
        private static Dictionary<char, double> Frequency = new Dictionary<char, double>
        { {'A', 8.17}, {'B', 1.49}, {'C', 2.78}, {'D', 4.25}, {'E', 12.7}, {'F', 2.23}, {'G', 2.02},
            {'H', 6.09}, {'I', 6.97}, {'J', 0.15}, {'K', 0.77}, {'L', 4.03 }, {'M', 2.41}, {'N', 6.75},
                {'O', 7.51}, {'P', 1.93}, {'Q', 0.1}, {'R', 5.99}, {'S', 6.33}, {'T', 9.06}, {'U', 2.76},
                    {'V', 0.98}, {'W', 2.36}, {'X', 0.15}, {'Y', 1.97}, {'Z', 0.07}
        };

        private static Dictionary<char, double> FrequencyInEncryptedText = new Dictionary<char, double>
        { {'A', 0.0}, {'B', 0.0}, {'C', 0.0}, {'D', 0.0}, {'E', 0.0}, {'F', 0.0}, {'G', 0.0},
            {'H', 0.0}, {'I', 0.0}, {'J', 0.0}, {'K', 0.0}, {'L', 0.0}, {'M', 0.0}, {'N', 0.0},
                {'O', 0.0}, {'P', 0.0}, {'Q', 0.0}, {'R', 0.0}, {'S', 0.0}, {'T', 0.0}, {'U', 0.0},
                    {'V', 0.0}, {'W', 0.0}, {'X', 0.0}, {'Y', 0.0}, {'Z', 0.0}
        };

        public static string DecryptText(string text)
        {
            CountLettersAmount(text, FrequencyInEncryptedText);

            return FormDecryptedText(text, FrequencyInEncryptedText);
        }

        private static void CountLettersAmount(string text, Dictionary<char, double> keyValues)
        {
            foreach (char symbol in text)
            {
                double currentValue;
                if (keyValues.TryGetValue(Char.ToUpper(symbol), out currentValue))
                {
                    keyValues[Char.ToUpper(symbol)] = currentValue + 1;
                }
            }
        }

        private static string FormDecryptedText(string encryptedText, Dictionary<char, double> frequencyInEncryptedText)
        {
            StringBuilder decryptedText = new StringBuilder(encryptedText.Length);

            foreach(char symbol in encryptedText)
            {
                if(Char.IsLetter(symbol))
                {
                    decryptedText.Append(GetDecryptKey(symbol, frequencyInEncryptedText));
                }
                else
                {
                    decryptedText.Append(symbol);
                }
            }

            return decryptedText.ToString();
        }

        private static char GetDecryptKey(char symbol, Dictionary<char, double> keyValues)
        {
            int index = 0;
            foreach (KeyValuePair<char, double> keyValue in Frequency.OrderBy(key => key.Value))
            {
                if (GetIndexByKey(symbol, keyValues) == index)
                {
                    return keyValue.Key;
                }
                index++;
            }

            return ' ';
        }

        private static int GetIndexByKey(char symbol, Dictionary<char, double> keyValues)
        {
            int index = 0;
            foreach(KeyValuePair<char, double> keyValue in keyValues.OrderBy(key => key.Value))
            {
                if(Char.ToUpper(symbol) == keyValue.Key)
                {
                    return index;
                }
                index++;
            }

            return index;
        }
    }
}

﻿using System;
using System.Text;

namespace Lab1
{
    class CaesarCipher
    {
        private const string abc = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        public static string EncryptText(string text, int offset = 3)
        {
            StringBuilder encryptedText = new StringBuilder(text.Length);

            foreach (char symbol in text)
            {
                if (Char.IsLetter(symbol))
                {
                    int index = abc.IndexOf(Char.ToUpper(symbol)) + offset;
                    encryptedText.Append(abc[index % abc.Length]);
                }
                else
                {
                    encryptedText.Append(symbol);
                }
            }
            return encryptedText.ToString();
        }

        public static string DecryptText(string encryptedText, int offset = 3)
        {
            StringBuilder decryptedText = new StringBuilder(encryptedText.Length);

            foreach (char symbol in encryptedText)
            {
                if (Char.IsLetter(symbol))
                {
                    int index = abc.IndexOf(Char.ToUpper(symbol)) - offset;
                    decryptedText.Append(abc[((index % abc.Length) + abc.Length) % abc.Length]);
                }
                else
                {
                    decryptedText.Append(symbol);
                }
            }
            return decryptedText.ToString();
        }
    }
}

﻿using System;
using System.IO;

namespace Lab1
{
    class Program
    {
        static void Main()
        {
            bool alive = true;

            while(alive)
            {
                Console.Title = "Cryptology Program";

                Console.WriteLine("Operations: 1. Encrypt text by Caesar Cipher 2. Decrypt text by Caesar Cipher" +
                    "\n3. Decrypt text by frequency analysis 4. Exit");

                try
                {
                    Console.WriteLine("Enter number of an operation: ");
                    int command = Convert.ToInt32(Console.ReadLine());

                    switch (command)
                    {
                        case 1: EncryptByCaesarOperation(); break;
                        case 2: DecryptByCaesarOperation(); break;
                        case 3: DecryptByFrequencyAnalysis(); break;
                        case 4: alive = false; break;
                        default: Console.WriteLine("Wrong number of operation. Please enter correct one."); break;
                    }
                }
                catch(Exception e)
                {
                     Console.WriteLine(e.Message);
                }
            }
        }

        private static void EncryptByCaesarOperation()
        {
            Console.WriteLine("Enter text for encryption: ");
            string text = Console.ReadLine();
            Console.WriteLine("Encrypted text: " + CaesarCipher.EncryptText(text));
            Console.WriteLine();
        }

        private static void DecryptByCaesarOperation()
        {
            Console.WriteLine("Enter text for decryption: ");
            string text = Console.ReadLine();
            Console.WriteLine("Decrypted text: " + CaesarCipher.DecryptText(text));
            Console.WriteLine();
        }

        private static void DecryptByFrequencyAnalysis()
        {
            Console.WriteLine("Enter path to file with encrypted text: ");
            string path = Console.ReadLine();
            string text = File.ReadAllText(path);
            
            File.AppendAllText(path, "\n\nDECRYPTED TEXT\n" + FrequencyAnalysis.DecryptText(text));
            Console.WriteLine("Decrypted successful!\n");
            
        }
    }
}
